package com.javagda23.zad1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String komenda;
        DateTimeFormatter formatter;

        do {
            System.out.println("Podaj komendę:");
            komenda = scanner.nextLine();

            switch (komenda) {
                case "data":
                    formatter = DateTimeFormatter.ofPattern("MM*dd*yyyy");
                    System.out.println(LocalDate.now().format(formatter));
                    break;
                case "czas":
                    formatter = DateTimeFormatter.ofPattern("HH-mm");
                    System.out.println(LocalTime.now().format(formatter));
                    break;
                case "dataiczas":
                    formatter = DateTimeFormatter.ofPattern("MM*dd*yyyy HH-mm");
                    System.out.println(LocalDateTime.now().format(formatter));
                    break;
                default:
                    break;
            }
//            if (komenda.equalsIgnoreCase("data")) {
//                // wypisanie daty w zadanym formacie
//                formatter = DateTimeFormatter.ofPattern("MM*dd*yyyy");
//                System.out.println(LocalDate.now().format(formatter));
//
//            } else if (komenda.equalsIgnoreCase("czas")) {
//                // wypisanie czasu w zadanym formacie
//                formatter = DateTimeFormatter.ofPattern("HH-mm");
//                System.out.println(LocalTime.now().format(formatter));
//
//            } else if (komenda.equalsIgnoreCase("dataiczas")) {
//                // wypisanie daty i czasu w zadanym formacie
//                formatter = DateTimeFormatter.ofPattern("MM*dd*yyyy HH-mm");
//                System.out.println(LocalDateTime.now().format(formatter));
//
//            }a

        } while (!komenda.equalsIgnoreCase("quit"));
    }
}
